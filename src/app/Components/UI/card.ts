import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `<div class="card">
    <div class="imgcontainer">
      <app-dialog>
        <app-button
          style="margin-top:0.5rem;"
          (clickEvent)="eventEmitterHandler('Ver')"
          width="4rem"
          >Ver</app-button
        >
        <app-button
          style="margin-top:0.5rem;"
          (clickEvent)="eventEmitterHandler('Add')"
          width="5rem"
          >Agregar</app-button
        >
      </app-dialog>
      <img [src]="imgsrc" alt="product" />
    </div>
    <h3>{{ description }}</h3>
    <h5>SKU: {{ code }}</h5>
    <h1>$ {{ price }}</h1>
    <h4>gramo a $ {{ price / 1000 }}</h4>
  </div>`,
  styleUrls: ['./card.scss'],
})
export class Card {
  @Input()
  description = '';

  @Input()
  imgsrc = '';

  @Input()
  price = 0;

  @Input()
  code = '';

  @Output()
  eventEmitter = new EventEmitter<string>();

  eventEmitterHandler(event: string) {
    this.eventEmitter.emit(event);
  }
}
