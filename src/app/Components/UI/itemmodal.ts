import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CartItem, Item } from 'src/app/entity/item';
import * as fromApp from '../../store/app.reducer';
import * as CartActions from '../../store/cart/cart.actions';

@Component({
  selector: 'app-item-modal',
  templateUrl: './itemmodal.html',
  styleUrls: ['./itemmodal.scss'],
})
export class ItemModal implements OnInit {
  cartSubscription: Subscription;
  authSubscription: Subscription;

  @Input()
  item: CartItem = null;

  token = '';

  counter = 0;

  isAuth = false;

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  ngOnInit() {
    this.authSubscription = this.store.select('auth').subscribe((authState) => {
      this.token = authState.token;
      this.isAuth = authState.isAuthenticated;
    });

    this.cartSubscription = this.store.select('cart').subscribe((cartState) => {
      this.counter = cartState.counter;
    });
  }

  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
    this.authSubscription.unsubscribe();
  }

  addHandler() {
    this.store.dispatch(new CartActions.AddToItem(this.item.item));
  }

  removeHandler() {
    this.store.dispatch(new CartActions.MinusToItem(this.item));
    if (this.counter === 0) {
      if (this.isAuth) {
        this.store.dispatch(new CartActions.SaveCart());
      } else {
        this.store.dispatch(
          new CartActions.OpenSpinner(
            'No se encuentra autentificado. Redireccionando ...'
          )
        );
        setTimeout(() => {
          this.store.dispatch(new CartActions.CloseSpinner());
          this.router.navigate(['/googleauth']);
        }, 200);
      }

      this.store.dispatch(new CartActions.SaveCart());
    }
  }
}
