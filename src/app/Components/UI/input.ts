import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  template: `<input
    [(ngModel)]="inputValue"
    (input)="onChangeHandler($event)"
    [placeholder]="placeholder"
  />`,
  styleUrls: ['./input.scss'],
})
export class CustomInput {
  @Input()
  placeholder = '';

  @Output()
  inputEvent = new EventEmitter<string>();

  inputValue: string = '';

  onChangeHandler(event: any) {
    this.inputEvent.emit(event.target.value);
  }
}
