import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-accordion',
  template: ` <div class="logoutButton">
    <button (click)="onClickHandler()" class="button">Logout</button>
  </div>`,
  styleUrls: ['./accordion.scss'],
})
export class Accordion {
  @Output()
  clickHandler = new EventEmitter<void>();

  @Input()
  visible = false;

  onClickHandler() {
    this.clickHandler.emit();
  }
}
