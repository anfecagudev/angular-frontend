import { Component, EventEmitter, Input, Output } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import * as ItemsActions from '../../store/items/items.actions';

@Component({
  selector: 'app-finder',
  template: `<div>
    <app-input
      (inputEvent)="inputChangeHandler($event)"
      [placeholder]="placeholder"
    ></app-input>
    <app-button [text]="'Icon of Search'">
      <fa-icon [icon]="faSearch"></fa-icon>
    </app-button>
  </div>`,
  styleUrls: ['./finder.scss'],
})
export class Finder {
  faSearch = faSearch;

  @Input()
  placeholder = '';

  @Output()
  inputChange = new EventEmitter<string>();


  constructor(private store: Store<fromApp.AppState>){}

  inputChangeHandler(input: string) {
    this.inputChange.emit(input);
  }
}
