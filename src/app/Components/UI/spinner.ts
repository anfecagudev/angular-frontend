import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-spinner',
  template: `<div class="spinnerContainer">
    <div class="messageContainer">
      <p>{{ message }}</p>
    </div>
    <div class="loader"></div>
  </div>`,
  styleUrls: ['./spinner.scss'],
})
export class Spinner implements OnInit, OnDestroy {
  message = '';

  subscription: Subscription;

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.subscription = this.store.select('cart').subscribe((ans) => {
      this.message = ans.spinnerMessage;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
