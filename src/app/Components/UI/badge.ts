import { Component } from '@angular/core';

@Component({
  selector: 'app-badge',
  template: `<div [class]="cssClasses.join(' ')">
    <ng-content></ng-content>
  </div>`,
  styleUrls: ['./badge.scss'],
})
export class Badge {
  cssClasses = ['badge'];
}
