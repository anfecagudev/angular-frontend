import { Component } from '@angular/core';

@Component({
  selector: 'app-dialog',
  template: ` <div class="dialog">
    <div class="backdrop"></div>
    <div class="content">
      <ng-content></ng-content>
    </div>
  </div>`,
  styleUrls: ['./dialog.scss'],
})
export class Dialog {}
