import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromApp from '../../store/app.reducer';
import * as CartActions from '../../store/cart/cart.actions';

@Component({
  selector: 'app-cart-button',
  template: ` <div class="cartlogo" (click)="openCartModal()">
    <app-badge class="badge-container" [@badge-bump]="bumper">{{ cartQuantity }}</app-badge>
    <img
      class="cartlogoimage"
      src="../../../assets/shopping-cart-icon.png"
      alt="CartLogo"
    />
  </div>`,
  styleUrls: ['./cartButton.scss'],
  animations: [
    trigger('badge-bump', [
      state('normal', style({ transform: 'scale(1)', backgroundColor: 'red' })),
      state(
        'bumping',
        style({ transform: 'scale(1.1)', backgroundColor: 'orange' })
      ),
      transition('normal <=> bumping', animate(150)),
    ]),
  ],
})
export class CartButton implements OnInit, OnDestroy {
  cartQuantity;

  subscription: Subscription;

  constructor(private store: Store<fromApp.AppState>) {}

  bumper = 'normal';

  timer;

  ngOnInit() {
    this.subscription = this.store
      .select('cart')
      .pipe(map((cartState) => cartState.counter))
      .subscribe((ans) => {
        clearTimeout(this.timer);
        this.cartQuantity = ans;
        this.bumper = 'bumping';
        this.timer = setTimeout(() => {
          this.bumper = 'normal';
        }, 300);
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  openCartModal() {
    this.store.dispatch(new CartActions.OpenCartModal());
  }
}
