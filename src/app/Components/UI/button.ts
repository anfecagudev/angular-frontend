import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  template: `<button (click)="onClickHandler()" [ngStyle]="styles">
    <ng-content></ng-content>
  </button>`,
  styleUrls: ['./button.scss'],
})
export class Button implements OnInit {
  @Input() text = '';

  @Input() width = '';

  @Input() height = '';

  @Output()
  clickEvent = new EventEmitter<void>();

  styles = {};

  ngOnInit() {
    this.styles = {
      width: this.width ? this.width : '2.5rem',
      height: this.height ? this.height : '2.5rem',
    };
  }

  onClickHandler(){
    this.clickEvent.emit();
  }

}
