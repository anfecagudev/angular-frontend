import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Button } from './button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  declarations: [Button],
  imports: [CommonModule, FormsModule, FontAwesomeModule],
  exports: [CommonModule, Button, FormsModule, FontAwesomeModule],
})
export class SharedModule {}
