import { Component } from "@angular/core";

@Component({
    selector: 'app-blank',
    template: `<template></template>`
})
export class Blank{}