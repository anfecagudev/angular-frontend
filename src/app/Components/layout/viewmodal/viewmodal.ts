import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartItem, Item } from 'src/app/entity/item';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../../../store/cart/cart.actions';

@Component({
  selector: 'app-viewmodal',
  templateUrl: './viewmodal.html',
  styleUrls: ['./viewmodal.scss'],
})
export class ViewModal implements OnInit, OnDestroy {
  cartSubscription: Subscription;
  authSubscription: Subscription;

  @Input()
  isVisible = false;

  @Input()
  alttext = '';
  quantity = 1;
  item: Item;
  isAuth = false;

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  ngOnInit() {
    this.cartSubscription = this.store
      .select('cart')
      .pipe(map((cartStore) => cartStore.item))
      .subscribe((ans) => (this.item = ans));

    this.authSubscription = this.store
      .select('auth')
      .pipe(map((authStore) => authStore.isAuthenticated))
      .subscribe((ans) => (this.isAuth = ans));
  }

  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
  }

  addHandler() {
    this.quantity++;
  }

  removeHandler() {
    if (this.quantity > 1) {
      this.quantity--;
    }
  }

  closeModal() {
    this.store.dispatch(new CartActions.CloseModal());
  }

  onAddToCart() {
    if (this.isAuth) {
      this.store.dispatch(
        new CartActions.AddToCart(new CartItem(this.item, this.quantity))
      );
      this.closeModal();
    } else {
      this.store.dispatch(
        new CartActions.OpenSpinner(
          'No se encuentra autentificado. Redireccionando'
        )
      );
      setTimeout(() => {
        this.store.dispatch(new CartActions.CloseSpinner());
        this.router.navigate(['/googleauth']);
      }, 1000);
    }
  }
}
