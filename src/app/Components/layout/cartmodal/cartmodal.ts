import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CartItem } from 'src/app/entity/item';
import * as fromApp from '../../../store/app.reducer';

@Component({
  selector: 'app-cartmodal',
  template: `<div class="modal">
    <app-item-modal
      class="item_container"
      *ngFor="let item of cartItems; trackBy: trackByFn"
      @itemAnimation
      [item]="item"
    ></app-item-modal>

    <hr class="hr" />
    <app-totalizer
      class="totalizer_container"
      [visible]="visible"
    ></app-totalizer>
  </div>`,
  styleUrls: ['./cartmodal.scss'],
  animations: [
    trigger('itemAnimation', [
      state('in', style({ transform: 'translateY(0)', opacity: 1 })),
      transition('void => *', [
        style({ transform: 'translateY(10rem)', opacity: 0 }),
        animate(300),
      ]),
      transition('* => void', [
        animate(300, style({ transform: 'translateY(-10rem)', opacity: 0 })),
      ]),
    ]),
  ],
})
export class CartModal implements OnInit, OnDestroy {
  @Input()
  visible: false;

  cartSubscription: Subscription;

  cartItems: CartItem[];

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.cartSubscription = this.store.select('cart').subscribe((ans) => {
      this.cartItems = ans.cartItems;
    });
  }

  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
  }

  trackByFn(_, item) {
    return item.code;
  }
}
