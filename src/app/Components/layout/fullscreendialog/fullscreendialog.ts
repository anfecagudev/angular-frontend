import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../../../store/cart/cart.actions';

@Component({
  selector: 'app-full-screen-dialog',
  template: `<div class="dialog">
    <div (click)="closeModal()" class="backdrop"></div>
    <ng-content></ng-content>
  </div>`,
  styleUrls: ['./fullscreendialog.scss'],
})
export class FullScreenDialog {
  constructor(private store: Store<fromApp.AppState>) {}
  // const store = useStore();
  // const cssClasses = ref(["dialog_entering"]);
  // setTimeout(() => {
  //   cssClasses.value = ["dialog"];
  // }, 20);
  // const premodal = computed(() => {
  //   return store.getters["cart/premodal"];
  // });
  // watch(premodal, (nval) => {
  //   if (nval) {
  //     cssClasses.value = ["dialog", "dialog_closing"];
  //   }
  // });
  closeModal() {
    this.store.dispatch(new CartActions.CloseModal());
  }
}

// <template>
//   <teleport to="body">
//
//   </teleport>
// </template>
