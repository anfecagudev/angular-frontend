import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as fromApp from '../../../store/app.reducer';
import * as AuthActions from '../../../store/auth/auth.actions';
import * as CartActions from '../../../store/cart/cart.actions';
import * as ItemsActions from '../../../store/items/items.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.html',
  styleUrls: ['./header.scss'],
  animations: [
    trigger('accordion-animation', [
      state('in', style({ transform: 'scaleY(1)', opacity: 1 })),
      transition('void => *', [
        style({ transform: 'scaleY(0)', opacity: 0 }),
        animate(300),
      ]),
      transition('* => void', [
        animate(300, style({ transform: 'scaleY(0)' })),
      ]),
    ]),
  ],
})
export class Header implements OnInit, OnDestroy {
  subscription: Subscription;
  isButtonVisible = false;
  closeAccordion = false;
  isAuthenticated = false;
  username = '';
  imgUrl = '';

  constructor(private router: Router, private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.subscription = this.store
      .select('auth')
      .pipe(
        map((authStore) => {
          return {
            isAuth: authStore.isAuthenticated,
            username: authStore.userName,
            imgUrl: authStore.imgUrl,
          };
        })
      )
      .subscribe((ans) => {
        this.isAuthenticated = ans.isAuth;
        this.username = ans.username;
        this.imgUrl = ans.imgUrl;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  logoutHandler() {
    localStorage.removeItem('token');
    this.store.dispatch(new AuthActions.Logout());
    this.store.dispatch(new CartActions.SetCart([]));
  }

  startSessionHandler() {
    this.router.navigate(['/googleauth']);
  }

  unhoverHandler() {
    this.closeAccordion = true;
    this.isButtonVisible = false;
  }

  hoverHandler() {
    this.closeAccordion = false;
    this.isButtonVisible = true;
  }

  onChangeHandler(input: any) {
    this.store.dispatch(new ItemsActions.FilterItemsByName(input));
  }
}
