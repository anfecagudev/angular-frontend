import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../store/app.reducer';
import * as ItemsActions from 'src/app/store/items/items.actions';


@Component({
  selector: 'app-sidebar',
  template: `
    <div id="sidebar" class="sidebar">
      <h1 class="sidebar-title">Secciones</h1>
      <button
        (click)="typeSelectionHandler(item)"
        class="sidebar-button"
        *ngFor="let item of items"
      >
        {{ item }}
      </button>
    </div>
  `,
  styleUrls: ['./sidebar.scss'],
})
export class Sidebar {
  items = ['Todos', 'Carnes', 'Tecnologia'];

  constructor(private store: Store<fromApp.AppState>){}

  typeSelectionHandler(item: string) {
    this.store.dispatch(new ItemsActions.FilterItemsByType(item));
  }
}
