import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-portrait',
  template: `<div class="portrait">
    <img [src]="imgUrl" alt="userportrait" />
    <p>{{ username }}</p>
  </div>`,
  styleUrls: ['./userportrait.scss'],
})
export class UserPortrait {
  @Input()
  imgUrl: string = '';

  @Input()
  username: string = '';
}
