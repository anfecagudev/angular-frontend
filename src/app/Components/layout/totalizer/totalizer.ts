import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { CartItem, DataBaseCartItem } from 'src/app/entity/item';
import * as fromApp from '../../../store/app.reducer';
import * as CartActions from '../../../store/cart/cart.actions';

@Component({
  selector: 'app-totalizer',
  template: ` <div class="container">
    <div class="shoppingBtn">
      <button (click)="shoppingHandler()" class="button">Comprar</button>
    </div>
    <div class="rightAligner">
      <div>Total:</div>
      <div>$ {{ total }}</div>
    </div>
  </div>`,
  styleUrls: ['./totalizer.scss'],
})
export class Totalizer implements OnInit, OnDestroy {
  @Input()
  visible = false;

  authSubscription: Subscription;

  cartSubscription: Subscription;

  token = '';

  isAuth = false;

  items: CartItem[];

  total = 0;

  constructor(private store: Store<fromApp.AppState>, private router: Router) {}

  ngOnInit() {
    this.authSubscription = this.store.select('auth').subscribe((authState) => {
      this.isAuth = authState.isAuthenticated;
      this.token = authState.token;
    });
    this.cartSubscription = this.store.select('cart').subscribe((cartState) => {
      this.items = cartState.cartItems;
      this.total = this.items.reduce(
        (prev, cur) => prev + cur.quantity * cur.item.price,
        0
      );
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

  shoppingHandler() {
    if (this.isAuth) {
      this.store.dispatch(new CartActions.SaveCart());
    } else {
      this.store.dispatch(
        new CartActions.OpenSpinner(
          'No se encuentra autentificado. Redireccionando ...'
        )
      );
      setTimeout(() => {
        this.store.dispatch(new CartActions.CloseSpinner());
        this.router.navigate(['/googleauth']);
      }, 200);
    }
  }
}
