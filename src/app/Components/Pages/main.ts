import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import * as fromApp from '../../store/app.reducer';
import { map } from 'rxjs/operators';
import * as ItemsActions from '../../store/items/items.actions';
import * as CartActions from '../../store/cart/cart.actions';
import * as AuthActions from '../../store/auth/auth.actions';
import { CartItem, Item } from 'src/app/entity/item';
import {
  trigger,
  transition,
  state,
  style,
  animate,
} from '@angular/animations';
import { faSleigh } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-main',
  templateUrl: './main.html',
  styleUrls: ['./main.scss'],

  animations: [
    trigger('modalAnimation', [
      state('in', style({ transform: 'translateY(0)', opacity: 1 })),
      transition('void <=> *', [
        style({ transform: 'translateY(-10rem)', opacity: 0 }),
        animate(300),
      ]),
      // transition('* => void', [
      //   animate(300, style({ transform: 'translateY(10rem)' })),
      // ]),
    ]),
  ],
})
export class Main implements OnInit, OnDestroy {
  private itemsSubscription: Subscription;
  private cartSubscription: Subscription;
  items = [];
  error = '';
  modal = '';
  timer;
  leavingStyle = {};
  isSpinner = false;

  constructor(private store$: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.store$.dispatch(new AuthActions.Login());
    new CartActions.OpenSpinner('Fetching Data...');
    this.store$.dispatch(new ItemsActions.FetchItems());
    this.store$.dispatch(new CartActions.FetchCart());
    this.store$
      .select('cart')
      .pipe(
        map((cartState) => {
          return { modal: cartState.modal, spinner: cartState.spinner };
        })
      )
      .subscribe((ans) => {
        this.isSpinner = ans.spinner;
        if (this.modal && !ans.modal) {
          this.leavingStyle = {
            transform: 'translateY(10rem)',
            opacity: 0,
            transition: 'all 300ms ease-out',
          };
          this.timer = setTimeout(() => {
            this.modal = ans.modal;
          }, 300);
        } else {
          this.leavingStyle = {};
          this.modal = ans.modal;
        }
      });

    this.itemsSubscription = this.store$
      .select('items')
      .pipe(
        map((itemsState) => {
          return {
            items: itemsState.filteredItemsByName,
            error: itemsState.error,
          };
        })
      )
      .subscribe((ans) => {
        this.items = ans.items;
        this.error = ans.error;
      });
  }

  ngOnDestroy() {
    setTimeout(this.timer);
    this.itemsSubscription.unsubscribe();
    this.cartSubscription.unsubscribe();
  }

  eventHandler(event: any, item: Item) {
    if (event === 'Ver') {
      this.store$.dispatch(new CartActions.OpenModal(item));
    } else {
      this.store$.dispatch(new CartActions.AddToCart(new CartItem(item, 1)));
    }
  }
}
