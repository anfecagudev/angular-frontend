import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import * as AuthActions from '../../store/auth/auth.actions';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-selector',
  template: `<template></template>`,
  styles: [],
})
export class Callback implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private store$: Store<fromApp.AppState>
  ) {}

  ngOnInit() {
    const token = this.route.snapshot.queryParams['token'];
    if (token) {
      localStorage.setItem('token', token);
      this.store$.dispatch(new AuthActions.Login());
    } else {
      const error = this.route.snapshot.queryParams['error'];
      this.store$.dispatch(new AuthActions.Error(error));
    }
  }
}
