import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Card } from '../UI/card';
import { Dialog } from '../UI/dialog';
import { Main } from './main';
import { SharedModule } from '../UI/shared.module';
import { FullScreenDialog } from '../layout/fullscreendialog/fullscreendialog';
import { ViewModal } from '../layout/viewmodal/viewmodal';
import { CartModal } from '../layout/cartmodal/cartmodal';
import { ItemModal } from '../UI/itemmodal';
import { Totalizer } from '../layout/totalizer/totalizer';
import { Spinner } from '../UI/spinner';

@NgModule({
  declarations: [
    Card,
    Dialog,
    Main,
    FullScreenDialog,
    ViewModal,
    CartModal,
    ItemModal,
    Totalizer,
    Spinner
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: Main }]),
  ],
})
export class MainModule {}
