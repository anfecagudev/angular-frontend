import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Callback } from './callback';

@NgModule({
  imports: [RouterModule.forChild([{ path: '', component: Callback }])],
})
export class CallbackModule {}
