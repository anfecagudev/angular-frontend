import { Action } from '@ngrx/store';
import { Item, CartItem, DataBaseCartItem } from '../../entity/item';

export const OPEN_MODAL = '[Cart] OpenModal';

export const CLOSE_MODAL = '[Cart] CloseModal';

export const ADD_TO_CART = '[Cart] AddToCart';

export const SET_CART = '[Cart] Set Cart';

export const ADD_TO_ITEM = '[Cart] Add To Item';

export const MINUS_TO_ITEM = '[Cart] Minus To Item';

export const OPEN_CART_MODAL = '[Cart] Open Cart Modal';

export const OPEN_SPINNER = '[Cart] Open Spinner';

export const CLOSE_SPINNER = '[Cart] Close Spinner';

export const FETCH_CART = '[Cart] Fetch Cart';

export const SAVE_CART = '[Cart] Save Cart';

export const SET_ERROR = '[Cart] Set Error';

export class OpenModal implements Action {
  readonly type = OPEN_MODAL;
  constructor(public payload: Item) {}
}

export class CloseModal implements Action {
  readonly type = CLOSE_MODAL;
}

export class AddToCart implements Action {
  readonly type = ADD_TO_CART;
  constructor(public payload: CartItem) {}
}

export class SetCart implements Action {
  readonly type = SET_CART;
  constructor(public payload: CartItem[]) {}
}

export class AddToItem implements Action {
  readonly type = ADD_TO_ITEM;
  constructor(public payload: Item) {}
}

export class MinusToItem implements Action {
  readonly type = MINUS_TO_ITEM;
  constructor(public payload: CartItem) {}
}

export class OpenCartModal implements Action {
  readonly type = OPEN_CART_MODAL;
}

export class OpenSpinner implements Action {
  readonly type = OPEN_SPINNER;
  constructor(public payload: string) {}
}

export class CloseSpinner implements Action {
  readonly type = CLOSE_SPINNER;
}

export class FetchCart implements Action {
  readonly type = FETCH_CART;
}

export class SaveCart implements Action {
  readonly type = SAVE_CART;
}

export class SetError implements Action {
  readonly type = SET_ERROR;
  constructor(public payload: string) {}
}

export type CartActions =
  | OpenModal
  | CloseModal
  | AddToCart
  | SetCart
  | AddToItem
  | MinusToItem
  | OpenCartModal
  | OpenSpinner
  | CloseSpinner
  | FetchCart
  | SaveCart
  | SetError;
