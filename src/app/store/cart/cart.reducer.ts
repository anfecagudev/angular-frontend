import { CartItem, Item } from 'src/app/entity/item';
import * as CartActions from './cart.actions';

export interface State {
  modal: string;
  item: Item;
  cartItems: CartItem[];
  counter: number;
  spinner: boolean;
  spinnerMessage: string;
  premodal: string;
  error: string;
  isError: boolean;
}

const initialState: State = {
  modal: '',
  item: null,
  cartItems: [],
  counter: 0,
  spinner: false,
  spinnerMessage: '',
  premodal: '',
  error: '',
  isError: false,
};

const addtoCounter = (cartItems: CartItem[]) => {
  return cartItems.reduce((prev, cur) => prev + cur.quantity, 0);
};

export function cartReducer(
  state: State = initialState,
  action: CartActions.CartActions
) {
  let newCartItems;
  let index;
  switch (action.type) {
    case CartActions.OPEN_MODAL:
      return {
        ...state,
        item: action.payload,
        modal: 'detail',
      };
    case CartActions.CLOSE_MODAL:
      return {
        ...state,
        modal: '',
      };
    case CartActions.ADD_TO_CART:
      index = state.cartItems.findIndex(
        (item) => item.item.code === action.payload.item.code
      );
      if (index >= 0) {
        const newItem = {
          ...state.cartItems[index],
          quantity: state.cartItems[index].quantity + action.payload.quantity,
        };
        newCartItems = [...state.cartItems];
        newCartItems.splice(index, 1, newItem);
        // const newCartItems = state.cartItems.filter(
        //   (item) => itemToChange.item.code !== itemToChange.item.code
        // );
        // newCartItems.push(newItem);
        return {
          ...state,
          cartItems: newCartItems,
          counter: addtoCounter(newCartItems),
        };
      } else {
        newCartItems = [...state.cartItems, action.payload];
        return {
          ...state,
          cartItems: newCartItems,
          counter: addtoCounter(newCartItems),
        };
      }
    case CartActions.SET_CART:
      return {
        ...state,
        cartItems: action.payload,
        counter: addtoCounter(action.payload),
        spinner: false,
        spinnerMessage: '',
      };
    case CartActions.ADD_TO_ITEM:
      newCartItems = [...state.cartItems];

      index = state.cartItems.findIndex(
        (item) => item.item.code === action.payload.code
      );

      const newItem = { ...newCartItems[index] };
      newItem.quantity = newItem.quantity + 1;

      newCartItems.splice(index, 1, newItem);

      return {
        ...state,
        cartItems: newCartItems,
        counter: addtoCounter(newCartItems),
      };
    case CartActions.MINUS_TO_ITEM:
      newCartItems = [...state.cartItems];
      index = state.cartItems.findIndex(
        (item) => item.item.code === action.payload.item.code
      );
      if (action.payload.quantity === 1) {
        newCartItems.splice(index, 1);
        return {
          ...state,
          cartItems: newCartItems,
          counter: addtoCounter(newCartItems),
        };
      } else {
        const newItem = { ...newCartItems[index] };
        newItem.quantity = newItem.quantity - 1;

        newCartItems.splice(index, 1, newItem);
        return {
          ...state,
          cartItems: newCartItems,
          counter: addtoCounter(newCartItems),
        };
      }
    case CartActions.OPEN_CART_MODAL:
      return {
        ...state,
        modal: 'cart',
      };
    case CartActions.OPEN_SPINNER:
      return {
        ...state,
        spinner: true,
        spinnerMessage: action.payload,
      };
    case CartActions.CLOSE_SPINNER:
      return {
        ...state,
        spinner: false,
        spinnerMessage: '',
      };
    case CartActions.SET_ERROR:
      return {
        ...state,
        isError: true,
        error: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
}
