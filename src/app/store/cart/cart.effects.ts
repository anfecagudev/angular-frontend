import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { combineLatest, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import * as CartActions from './cart.actions';
import * as fromApp from '../app.reducer';
import { CartItem, DataBaseCartItem } from 'src/app/entity/item';

@Injectable()
export class CartEffects {
  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) {}

  saveCart = createEffect(() =>
    this.actions$.pipe(
      ofType(CartActions.SAVE_CART),
      withLatestFrom(
        combineLatest([this.store.select('cart'), this.store.select('auth')])
      ),
      switchMap(([_, recipesState]) => {
        const products = recipesState[0].cartItems.map((item) => {
          return new DataBaseCartItem(item.item, item.quantity);
        });
        return this.http
          .post<any>('http://localhost:8081/cart/savecart', products, {
            headers: {
              Authorization: `Bearer ${recipesState[1].token}`,
              'Content-Type': 'application/json',
            },
          })
          .pipe(
            map((ans) => {
              const cartItems = ans.products.map((prodI) => {
                return new CartItem(prodI.productItem, prodI.quantity);
              });
              return new CartActions.SetCart(cartItems);
            }),
            catchError((err) => {
              return of(new CartActions.SetError(err.message));
            }),
            map(() => {
              return new CartActions.CloseModal();
            })
          );
      })
    )
  );

  fetchCart = createEffect(() =>
    this.actions$.pipe(
      ofType(CartActions.FETCH_CART),
      withLatestFrom(this.store.select('auth')),
      switchMap(([_, recipesState]) => {
        return this.http
          .get<any>('http://localhost:8081/cart/cart', {
            headers: {
              Authorization: `Bearer ${recipesState.token}`,
              'Content-Type': 'application/json',
            },
          })
          .pipe(
            map((ans) => {
              const cartItems = ans.products.map((prodI) => {
                return new CartItem(prodI.productItem, prodI.quantity);
              });
              return new CartActions.SetCart(cartItems);
            }),
            catchError((err) => {
              return of(new CartActions.SetError(err.message));
            })
          );
      })
    )
  );
}
