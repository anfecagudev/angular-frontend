import * as AuthActions from './auth.actions';

export interface State {
  isAuthenticated: boolean;
  token: string;
  imgUrl: string;
  userName: string;
  isError: boolean;
  error: string;
}

const initialState: State = {
  isAuthenticated: false,
  token: '',
  imgUrl: '',
  userName: '',
  isError: false,
  error: '',
};

export function authReducer(
  state: State = initialState,
  action: AuthActions.AuthActions
) {
  switch (action.type) {
    case AuthActions.AUTHENTICATE:
      return {
        ...state,
        isAuthenticated: true,
        token: action.payload.token,
        imgUrl: action.payload.imgUrl,
        userName: action.payload.userName,
        error: '',
        isError: false,
      };
    case AuthActions.LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        token: '',
        imgUrl: '',
        userName: '',
        error: '',
        isError: false,
      };
    case AuthActions.ERROR:
      return {
        ...state,
        error: action.payload,
        isError: true,
      };
    default:
      return {
        ...state,
      };
  }
}
