import { Action } from '@ngrx/store';

export const LOGOUT = '[auth] Logout';
export const AUTHENTICATE = '[auth] authenticate';
export const LOGIN = '[auth] Login';
export const ERROR = '[auth] Error';

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class Authenticate implements Action {
  readonly type = AUTHENTICATE;
  constructor(
    public payload: { token: string; imgUrl: string; userName: string }
  ) {}
}

export class Login implements Action {
  readonly type = LOGIN;
}

export class Error implements Action{
  readonly type = ERROR;
  constructor(public payload: string) {}
}

export type AuthActions = Authenticate | Logout | Login | Error;
