import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import * as fromApp from '../app.reducer';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  timer;

  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<fromApp.AppState>
  ) {}

  autoLogin = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.LOGIN),
      map(() => {
        const token = localStorage.getItem('token');
        if(token){
          clearTimeout(this.timer);
          const payload = JSON.parse(
            decodeURIComponent(escape(window.atob(token.split('.')[1])))
          );
          const remainingTime = payload.exp * 1000 - new Date().getTime();
          this.timer = setTimeout(() => {
            localStorage.removeItem('token');
            this.store.dispatch(new AuthActions.Logout());
          }, remainingTime);
          return new AuthActions.Authenticate({
            userName: payload.username,
            token: token,
            imgUrl: payload.imgUrl,
          });
        }
        else{
          return new AuthActions.Logout();
        }
        
      }),
      tap(() => {
        this.router.navigate(['/welcome']);
      })
    )
  );
}
