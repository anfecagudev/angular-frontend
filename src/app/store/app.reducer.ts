import { ActionReducerMap } from '@ngrx/store';
import * as fromAuth from './auth/auth.reducer';
import * as fromItems from './items/items.reducer';
import * as fromCart from './cart/cart.reducer';

export interface AppState {
  auth: fromAuth.State;
  items: fromItems.State;
  cart: fromCart.State;
}

export const appReducer: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  items: fromItems.ItemsReducer,
  cart: fromCart.cartReducer,
};
