import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import * as fromApp from '../app.reducer';
import { Store } from '@ngrx/store';
import * as ItemsActions from './items.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Item } from 'src/app/entity/item';
import { of } from 'rxjs';

@Injectable()
export class ItemsEffects {
  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store<fromApp.AppState>
  ) {}

  fetchItems = createEffect(() =>
    this.actions$.pipe(
      ofType(ItemsActions.FETCH_ITEMS),
      switchMap(() => {
        return this.http
          .get<Item[]>('http://localhost:8081/users/datalist')
          .pipe(
            map((response) => {
              return new ItemsActions.SetItems(response);
            }),
            catchError((error) => {
              return of(new ItemsActions.SetError(error.message));
            })
          );
      })
    )
  );
}
