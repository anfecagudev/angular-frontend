import { Action } from '@ngrx/store';
import { Item } from 'src/app/entity/item';

export const FETCH_ITEMS = '[items] Fetch Items';

export const SET_ITEMS = '[items] Set Items';

export const FILTER_ITEMS_BY_TYPE = '[items] Filter Items By Type';

export const FILTER_ITEMS_BY_NAME = '[items] Filter Items By Name';

export const SET_ERROR = '[items] Set Error';


export class FetchItems implements Action{
    readonly type = FETCH_ITEMS;    
}

export class SetItems implements Action {
  readonly type = SET_ITEMS;
  constructor(public payload: Item[]) {}
}

export class FilterItemsByType implements Action {
  readonly type = FILTER_ITEMS_BY_TYPE;
  constructor(public payload: string) {}
}

export class FilterItemsByName implements Action {
  readonly type = FILTER_ITEMS_BY_NAME;
  constructor(public payload: string) {}
}

export class SetError implements Action {
  readonly type = SET_ERROR;
  constructor(public payload: any) {}
}

export type ItemsActions = SetItems | FilterItemsByName | FilterItemsByType | FetchItems | SetError
