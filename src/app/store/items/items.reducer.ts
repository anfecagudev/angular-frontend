import { InitialState } from '@ngrx/store/src/models';
import { Item } from 'src/app/entity/item';
import * as ItemsActions from './items.actions';

const filterByType = (state, payload) => {
  return state.items.filter((item) => item.type === payload);
};

export interface State {
  type: string;
  items: Item[];
  filteredItemsByType: Item[];
  filteredItemsByName: Item[];
  error: any;
}

const initialState: State = {
  type: '',
  items: [],
  filteredItemsByType: [],
  filteredItemsByName: [],
  error: '',
};

export function ItemsReducer(
  state = initialState,
  action: ItemsActions.ItemsActions
) {
  switch (action.type) {
    case ItemsActions.SET_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case ItemsActions.SET_ITEMS:
      return {
        ...state,
        items: action.payload,
        filteredItemsByType: action.payload,
        filteredItemsByName: action.payload,
        error: '',
      };
    case ItemsActions.FILTER_ITEMS_BY_NAME:
      let filteredItemsByName;
      if (action.payload || action.payload === '') {
        filteredItemsByName = state.filteredItemsByType.filter((item) =>
          item.description.toLowerCase().includes(action.payload.toLowerCase())
        );
      } else {
        filteredItemsByName = state.filteredItemsByType;
      }
      return {
        ...state,
        filteredItemsByName,
      };
    case ItemsActions.FILTER_ITEMS_BY_TYPE:
      if (!action.payload || action.payload === 'Todos') {
        return {
          ...state,
          filteredItemsByName: state.items,
          filteredItemsByType: state.items,
          type: 'Todos',
        };
      } else {
        return {
          ...state,
          filteredItemsByName: filterByType(state, action.payload),
          filteredItemsByType: filterByType(state, action.payload),
          type: action.payload,
        };
      }
    default:
      return {
        ...state,
      };
  }
}
