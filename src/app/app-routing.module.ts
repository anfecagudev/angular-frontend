import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { Blank } from './Components/UI/blank';

const appRoutes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  {
    path: 'welcome',
    loadChildren: () =>
      import('./Components/Pages/main.module').then((m) => m.MainModule),
  },
  {
    path: 'callback',
    loadChildren: () =>
      import('./Components/Pages/callback.module').then((m) => m.CallbackModule),
  },
  {
    path: 'googleauth',
    component: Blank,
    resolve: {
        url: 'externalUrlRedirectResolver'
    },
    data: {
        externalUrl: 'http://localhost:8081/oauth2/authorize/google?redirect_uri=http://localhost:4200/callback'
    }
}
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
