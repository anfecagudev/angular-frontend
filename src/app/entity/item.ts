export class Item {
  id: string;
  imageUrl: string;
  type: string;
  code: string;
  unit: string;
  price: number;
  description: string;
}

export class CartItem {
  constructor(public item: Item, public quantity: number) {}
}

export class DataBaseCartItem{
  constructor(public productItem: Item, public quantity: number){}
}
