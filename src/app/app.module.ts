import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './Components/UI/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { Header } from './Components/layout/header/header';
import { Sidebar } from './Components/layout/sidebar/sidebar';
import { CartButton } from './Components/UI/cartButton';
import { Finder } from './Components/UI/finder';
import { CustomInput } from './Components/UI/input';
import { Badge } from './Components/UI/badge';
import { UserPortrait } from './Components/layout/UserPortrait/userportrait';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';

import { ItemsEffects } from './store/items/items.effects';
import { AuthEffects } from './store/auth/auth.effects';
import * as fromApp from './store/app.reducer';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Accordion } from './Components/UI/accordion';
import { CartEffects } from './store/cart/cart.effects';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    Header,
    Sidebar,
    CartButton,
    Finder,
    CustomInput,
    Badge,
    UserPortrait,
    Accordion
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FontAwesomeModule,
    StoreModule.forRoot(fromApp.appReducer),
    EffectsModule.forRoot([ItemsEffects, AuthEffects, CartEffects]),
  ],
  providers: [
    {
      provide: 'externalUrlRedirectResolver',
      useValue: (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
        window.location.href = (route.data as any).externalUrl;
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
